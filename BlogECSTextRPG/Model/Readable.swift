//
//  Readable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Readable {
    
    // MARK: - Fields
    static let component_id: Int64 = 10
    static let table = Table("Readables")
    static let readable_id_column = Expression<Int64>("id")
    static let content_column = Expression<String?>("content")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Readable.readable_id_column]
        }
    }
    
    var content: String? {
        get {
            return row[Readable.content_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Readable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getReadable() -> Readable? {
        guard let component = getComponent(Readable.component_id) else { return .None }
        let table = Readable.table.filter(Readable.readable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Readable(row: result)
    }
}