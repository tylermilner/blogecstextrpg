//
//  Entity.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/12/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Entity {
    
    // MARK: - Fields
    static let table = Table("Entities")
    static let entity_id_column = Expression<Int64>("id")
    static let label_column = Expression<String?>("label")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Entity.entity_id_column]
        }
    }
    
    var label: String? {
        get {
            return row[Entity.label_column]
        }
    }
}

extension Entity {
    func getComponents(componentID: Int64) -> [EntityComponent] {
        return EntityComponent.fetchByEntityID(id, componentID: componentID)
    }
    
    func getComponent(componentID: Int64) -> EntityComponent? {
        return EntityComponent.fetchByEntityID(id, componentID: componentID).first
    }
}

extension Entity {
    static func fetchByID(id: Int64) -> Entity? {
        let table = Entity.table.filter(Entity.entity_id_column == id)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Entity(row: result)
    }
    
    static func fetchByComponentID(componentID: Int64, dataID: Int64) -> Entity? {
        let componentTable = EntityComponent.table.filter(EntityComponent.component_id_column == componentID && EntityComponent.component_data_id_column == dataID)
        guard let result = DataManager.instance.prepare(componentTable).first else { return .None }
        let component = EntityComponent(row: result)
        return Entity.fetchByID(component.entityID)
    }
    
    static func fetchAllByComponentID(componentID: Int64, dataID: Int64) -> [Entity] {
        let componentTable = EntityComponent.table.filter(EntityComponent.component_id_column == componentID && EntityComponent.component_data_id_column == dataID)
        let matches = DataManager.instance.prepare(componentTable).map({ $0[EntityComponent.entity_id_column] })
        let entityTable = Entity.table.filter(matches.contains(Entity.entity_id_column))
        let result = DataManager.instance.prepare(entityTable).map({ Entity(row: $0) })
        return result
    }
}
