//
//  Eatable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Eatable {
    
    // MARK: - Fields
    static let component_id: Int64 = 12
    static let table = Table("Eatables")
    static let eatable_id_column = Expression<Int64>("id")
    static let message_column = Expression<String?>("message")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Eatable.eatable_id_column]
        }
    }
    
    var message: String? {
        get {
            return row[Eatable.message_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Eatable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getEatable() -> Eatable? {
        guard let component = getComponent(Eatable.component_id) else { return .None }
        let table = Eatable.table.filter(Eatable.eatable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Eatable(row: result)
    }
}
