//
//  EntityComponent.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/12/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

import Foundation
import SQLite

struct EntityComponent {
    
    // MARK: - Fields
    static let table = Table("EntityComponents")
    static let id_column = Expression<Int64>("id")
    static let entity_id_column = Expression<Int64>("entity_id")
    static let component_id_column = Expression<Int64>("component_id")
    static let component_data_id_column = Expression<Int64>("component_data_id")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[EntityComponent.id_column]
        }
    }
    
    var entityID: Int64 {
        get {
            return row[EntityComponent.entity_id_column]
        }
    }
    
    var componentID: Int64 {
        get {
            return row[EntityComponent.component_id_column]
        }
    }
    
    var dataID: Int64 {
        get {
            return row[EntityComponent.component_data_id_column]
        }
    }
}

extension EntityComponent {
    static func fetchByEntityID(entityID: Int64, componentID: Int64) -> [EntityComponent] {
        let table = EntityComponent.table.filter(EntityComponent.entity_id_column == entityID && EntityComponent.component_id_column == componentID)
        let result = DataManager.instance.prepare(table).map({ EntityComponent(row: $0) })
        return result
    }
}