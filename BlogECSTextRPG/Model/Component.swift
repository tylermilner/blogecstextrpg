//
//  Component.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/12/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Component {
    
    // MARK: - Fields
    static let table = Table("Components")
    static let component_id_column = Expression<Int64>("id")
    static let name_column = Expression<String>("name")
    static let description_column = Expression<String>("description")
    static let table_name_column = Expression<String>("table_name")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Component.component_id_column]
        }
    }
    
    var name: String {
        get {
            return row[Component.name_column]
        }
    }
    
    var description: String {
        get {
            return row[Component.description_column]
        }
    }
    
    var tableName: String {
        get {
            return row[Component.table_name_column]
        }
    }
}

extension Component {
    static func fetchByID(id: Int64) -> Component? {
        let table = Component.table.filter(Component.component_id_column == id)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Component(row: result)
    }
}
