//
//  Containable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Containable {
    
    // MARK: - Fields
    static let component_id: Int64 = 4
    static let table = Table("Containables")
    static let containable_id_column = Expression<Int64>("id")
    static let containing_entity_id_column = Expression<Int64>("containing_entity_id")
    static let size_column = Expression<Double>("size")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Containable.containable_id_column]
        }
    }
    
    var containingEntityID: Int64 {
        get {
            return row[Containable.containing_entity_id_column]
        } set(newValue) {
            
        }
    }
    
    var containingEntity: Entity? {
        get {
            return Entity.fetchByID(self.containingEntityID)
        }
    }
    
    var size: Double {
        get {
            return row[Containable.size_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Containable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getContainable() -> Containable? {
        guard let component = getComponent(Containable.component_id) else { return .None }
        let table = Containable.table.filter(Containable.containable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Containable(row: result)
    }
}
