//
//  TargetingFilter.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

public struct TargetingFilter : OptionSetType{
    public let rawValue : Int
    public init(rawValue:Int){ self.rawValue = rawValue}
    
    // Require that all candidates appear in the room that the player is located within
    static let CurrentRoom = TargetingFilter(rawValue: 1 << 2)
    
    // Require that any candidate contained by an openable container has the container open
    static let ContainerIsOpen = TargetingFilter(rawValue: 1 << 3)
    
    // Require that all candidates be in the player's inventory (or be contained by an object therein)
    static let HeldByPlayer = TargetingFilter(rawValue: 1 << 4)
    
    // Require that all candidates NOT be in the player's inventory (or be contained by an object therein)
    static let NotHeldByPlayer = TargetingFilter(rawValue: 1 << 5)
}
