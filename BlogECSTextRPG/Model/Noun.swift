//
//  Noun.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Noun {
    
    // MARK: - Fields
    static let component_id: Int64 = 6
    static let table = Table("Nouns")
    static let noun_id_column = Expression<Int64>("id")
    static let text_column = Expression<String>("text")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Noun.noun_id_column]
        }
    }
    
    var text: String {
        get {
            return row[Noun.text_column]
        }
    }
}

extension Entity {
    func getNouns() -> [Noun] {
        let components = getComponents(Noun.component_id)
        let ids = components.map({ $0.dataID })
        let table = Noun.table.filter(ids.contains(Noun.noun_id_column))
        let result = DataManager.instance.prepare(table).map({ Noun(row: $0) })
        return result
    }
}