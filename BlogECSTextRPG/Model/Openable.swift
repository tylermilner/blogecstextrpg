//
//  Openable.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Openable {
    
    // MARK: - Fields
    static let component_id: Int64 = 7
    static let table = Table("Openables")
    static let openable_id_column = Expression<Int64>("id")
    static let is_open_column = Expression<Bool>("is_open")
    static let is_locked_column = Expression<Bool>("is_locked")
    static let open_message_column = Expression<String?>("open_message")
    static let open_trigger_column = Expression<Int64?>("open_trigger_id")
    static let close_message_column = Expression<String?>("close_message")
    static let close_trigger_column = Expression<Int64?>("close_trigger_id")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Openable.openable_id_column]
        }
    }
    
    var isOpen: Bool {
        get {
            return row[Openable.is_open_column]
        }
    }
    
    var isLocked: Bool {
        get {
            return row[Openable.is_locked_column]
        }
    }
    
    var openMessage: String? {
        get {
            return row[Openable.open_message_column]
        }
    }
    
    var openTriggerID: Int64? {
        get {
            return row[Openable.open_trigger_column]
        }
    }
    
    var openTrigger: Entity? {
        get {
            guard let triggerID = openTriggerID else { return .None }
            return Entity.fetchByID(triggerID)
        }
    }
    
    var closeMessage: String? {
        get {
            return row[Openable.close_message_column]
        }
    }
    
    var closeTriggerID: Int64? {
        get {
            return row[Openable.close_trigger_column]
        }
    }
    
    var closeTrigger: Entity? {
        get {
            guard let triggerID = closeTriggerID else { return .None }
            return Entity.fetchByID(triggerID)
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Openable.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getOpenable() -> Openable? {
        guard let component = getComponent(Openable.component_id) else { return .None }
        let table = Openable.table.filter(Openable.openable_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Openable(row: result)
    }
}
