//
//  Container.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

struct Container {
    
    // MARK: - Fields
    static let component_id: Int64 = 3
    static let table = Table("Containers")
    static let container_id_column = Expression<Int64>("id")
    static let capacity_column = Expression<Double>("capacity")
    let row: Row
    
    // MARK: - Properties
    var id: Int64 {
        get {
            return row[Container.container_id_column]
        }
    }
    
    var capacity: Double {
        get {
            return row[Container.capacity_column]
        }
    }
    
    var entity: Entity? {
        get {
            return Entity.fetchByComponentID(Container.component_id, dataID: id)
        }
    }
}

extension Entity {
    func getContainer() -> Container? {
        guard let component = getComponent(Container.component_id) else { return .None }
        let table = Container.table.filter(Container.container_id_column == component.dataID)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Container(row: result)
    }
}
