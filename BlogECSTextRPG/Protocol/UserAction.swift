//
//  UserAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

protocol UserAction {
    var commands: Set<String> { get }
    var specifiers: Set<String> { get }
    func handle(interpretation: Interpretation)
}
