//
//  GameViewController.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/8/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private var historyTextView: UITextView!
    @IBOutlet private var commandTextField: UITextField!
    @IBOutlet private var stackBottomConstraint: NSLayoutConstraint!
    
    // MARK: - Fields
    private let bottomPadding: CGFloat = 16
    
    // MARK: - UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        MasterSystem.setup()
        historyTextView.text = LoggingSystem.instance.print()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow (_:)), name: UIKeyboardWillShowNotification, object: .None)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide (_:)), name: UIKeyboardWillHideNotification, object: .None)
        commandTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: .None)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: .None)
    }
    
    // MARK: - Notification Handlers
    func keyboardWillShow (notification: NSNotification) {
        guard let userInfo = notification.userInfo, keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.CGRectValue() else { return }
        stackBottomConstraint.constant = keyboardSize.height + bottomPadding
    }
    
    func keyboardWillHide (notification: NSNotification) {
        stackBottomConstraint.constant = bottomPadding
    }
    
    // MARK: - Private
    private func scrollToCurrent() {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue()) { [weak self] in
            guard let textView = self?.historyTextView else { return }
            let offset = CGPointMake(0, max(textView.contentSize.height - textView.bounds.height, 0))
            textView.setContentOffset(offset, animated: true)
        }
    }
}

extension GameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        guard let input = textField.text, message = String(UTF8String: "\n> \(input)") where input.characters.count > 0 else { return true }
        LoggingSystem.instance.addLog(message)
        InterpreterSystem.instance.handleUserInput(input)
        textField.text = "";
        historyTextView.text = LoggingSystem.instance.print()
        scrollToCurrent()
        return true
    }
}