//
//  TakeableSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class TakeableSystem {
    class func setup() {
        InterpreterSystem.instance.register(TakeAction())
        InterpreterSystem.instance.register(PutAction())
        InterpreterSystem.instance.register(DropAction())
    }
    
    class func take(takeable: Takeable) -> String {
        guard let containable = takeable.entity?.getContainable(), inventory = PlayerSystem.player.getContainer() where takeable.canTake else {
            return takeable.message ?? "You can't take that."
        }
        
        guard ContainmentSystem.canContain(containable, container: inventory) else {
            return "You can't hold any more."
        }
        
        ContainmentSystem.move(containable, containingEntityID: PlayerSystem.player.id)
        return takeable.message ?? "Taken."
    }
    
    class func put(takeable: Takeable, container: Container) -> String {
        guard let containable = takeable.entity?.getContainable(), containerID = container.entity?.id else {
            return "You can't do that."
        }
        
        guard ContainmentSystem.canContain(containable, container: container) else {
            return "It wont fit."
        }
        
        ContainmentSystem.move(containable, containingEntityID: containerID)
        return "Done."
    }
    
    class func drop(takeable: Takeable) -> String {
        guard let containable = takeable.entity?.getContainable(), roomID = RoomSystem.room.entity?.id else { return "You can't do that." }
        ContainmentSystem.move(containable, containingEntityID: roomID)
        return "Done."
    }
}
