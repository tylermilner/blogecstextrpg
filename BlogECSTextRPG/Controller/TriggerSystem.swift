//
//  TriggerSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class TriggerSystem {
    class func execute(trigger: Entity?) {
        guard let trigger = trigger else { return }
        let valueChangers = trigger.getValueChangers()
        for vc in valueChangers {
            apply(vc)
        }
    }
    
    class func apply(valueChanger: ValueChanger) {
        guard let component = Component.fetchByID(valueChanger.componentID) else { return }
        let idExpression = Expression<Int64>("id")
        let componentTable = Table(component.tableName)
        var update: Update?
        if let changeInt = valueChanger.changeInt {
            let columnExpression = Expression<Int64>(valueChanger.columnName)
            update = componentTable.filter(idExpression == valueChanger.componentDataID).update(columnExpression <- changeInt)
        } else if let changeText = valueChanger.changeText {
            let columnExpression = Expression<String>(valueChanger.columnName)
            update = componentTable.filter(idExpression == valueChanger.componentDataID).update(columnExpression <- changeText)
        }
        if let update = update {
            DataManager.instance.run(update)
        }
    }
}
