//
//  RoomSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/19/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class RoomSystem {
    static var room: Room {
        get {
            guard let roomID = PlayerSystem.player.getPresences().first?.roomID, result = RoomSystem.fetchByID(roomID) else { fatalError("The player must appear in a room") }
            return result
        }
    }
    
    class func fetchByID(id: Int64) -> Room? {
        let table = Room.table.filter(Room.room_id_column == id)
        guard let result = DataManager.instance.prepare(table).first else { return .None }
        return Room(row: result)
    }
    
    class func isPresentInCurrentRoom(entity: Entity) -> Bool {
        if let containable = entity.getContainable(), roomEntityID = RoomSystem.room.entity?.id, containerEntity = ContainmentSystem.rootEntity(containable) {
            if containerEntity.id == roomEntityID {
                return true
            }
            if let _ = containerEntity.getPresences().filter({ $0.roomID == room.id }).first {
                return true
            }
            return false
        }
        
        if let _ = entity.getPresences().filter({ $0.roomID == room.id }).first {
            return true
        }
        return false
    }
    
    class func move(roomID: Int64) {
        guard let presence = PlayerSystem.player.getPresences().first else { fatalError("Expected presence component on player") }
        guard let room = RoomSystem.fetchByID(roomID) else { fatalError("Invalid portal target") }
        move(presence, roomID: roomID)
        SightSystem.describeRoom(room, verbose: false)
        visit(room)
    }
    
    class func move(presence: Presence, roomID: Int64) {
        let update = Presence.table.filter(Presence.presence_id_column == presence.id).update(Presence.room_id_column <- roomID)
        DataManager.instance.run(update)
    }
    
    class func visit(room: Room) {
        let update = Room.table.filter(Room.room_id_column == room.id).update(Room.visited_column <- true)
        DataManager.instance.run(update)
    }
    
    class func entitiesInRoom() -> [Entity] {
        var result = entitiesPresentInRoom()
        result.appendContentsOf(entitiesContainedByRoom())
        return result
    }
    
    // MARK: - Private
    private class func entitiesPresentInRoom() -> [Entity] {
        let table = Presence.table.filter(Presence.room_id_column == RoomSystem.room.id)
        let presences = DataManager.instance.prepare(table).map({ Presence(row: $0) })
        var result: [Entity] = []
        for item in presences {
            guard let entity = item.entity else { continue }
            result.append(entity)
        }
        return result
    }
    
    private class func entitiesContainedByRoom() -> [Entity] {
        guard let roomEntity = room.entity else { return [] }
        return ContainmentSystem.fetchContainedEntities(roomEntity)
    }
}
