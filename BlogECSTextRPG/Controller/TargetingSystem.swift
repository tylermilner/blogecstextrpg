//
//  TargetingSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class TargetingSystem {
    class func filter(target: Target, options: TargetingFilter) {
        
        if target.candidates.count == 0 {
            target.error = "I don't know what a \(target.userInput) is."
            return
        }
        
        if options.contains(.CurrentRoom) {
            target.candidates = target.candidates.filter({ RoomSystem.isPresentInCurrentRoom($0) })
            if target.candidates.count == 0 {
                target.error = "You can't see any \(target.userInput) here."
                return
            }
        }
        
        if options.contains(.ContainerIsOpen) {
            target.candidates = target.candidates.filter({ (entity) -> Bool in
                if let container = entity.getContainable()?.containingEntity?.getOpenable() {
                    return container.isOpen
                }
                return true
            })
            if target.candidates.count == 0 {
                target.error = "You can't see any \(target.userInput) here."
                return
            }
        }
        
        if options.contains(.HeldByPlayer) {
            target.candidates = target.candidates.filter({ (entity) -> Bool in
                return PlayerSystem.heldByPlayer(entity.getContainable())
            })
            if target.candidates.count == 0 {
                target.error = "You don't have the \(target.userInput)."
            }
        }
        
        if options.contains(.NotHeldByPlayer) {
            target.candidates = target.candidates.filter({ (entity) -> Bool in
                return !PlayerSystem.heldByPlayer(entity.getContainable())
            })
            if target.candidates.count == 0 {
                target.error = "You already have the \(target.userInput)!"
                return
            }
        }
    }
    
    class func validate(target: Target) {
        guard target.error == .None else { return }
        switch target.candidates.count {
        case 0:
            target.error = "You can't see any \(target.userInput) here."
            break
        case 1:
            target.match = target.candidates.first
            break
        default:
            let candidateDescriptions = target.candidates.map({ LabelSystem.describe($0) }).joinWithSeparator(" or ")
            target.error = "Do you mean the \(candidateDescriptions)?"
            break
        }
    }
}
