//
//  MasterSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class MasterSystem {
    class func setup() {
        GameSystem.setup()
        SightSystem.setup()
        PortalSystem.setup()
        OpenableSystem.setup()
        PlayerSystem.setup()
        TakeableSystem.setup()
        ReadableSystem.setup()
        EatableSystem.setup()
        DrinkableSystem.setup()
    }
}
