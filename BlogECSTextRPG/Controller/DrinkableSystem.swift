//
//  DrinkableSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class DrinkableSystem {
    class func setup() {
        InterpreterSystem.instance.register(DrinkAction())
    }
    
    class func drink(drinkable: Drinkable) -> String {
        if let containable = drinkable.entity?.getContainable() {
            ContainmentSystem.move(containable, containingEntityID: 0)
        }
        
        let message = drinkable.message ?? "Done."
        return message
    }
}
