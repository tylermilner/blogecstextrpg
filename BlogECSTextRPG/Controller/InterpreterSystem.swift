//
//  InterpreterSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class InterpreterSystem {
    
    // MARK: - Singleton
    static let instance = InterpreterSystem()
    
    // MARK: - Fields
    private var verbs: Set<String> = []
    private lazy var verbRegex: String = {
        return "\\b\(self.verbs.joinWithSeparator("\\b|\\b"))\\b"
    }()
    
    private var prepositions: Set<String> = []
    private lazy var prepositionRegex: String = {
        return "\\b\(self.prepositions.joinWithSeparator("\\b|\\b"))\\b"
    }()
    
    private var handlers: [UserAction] = []
    
    // MARK: - Public
    func register(action: UserAction) {
        verbs.unionInPlace(action.commands)
        prepositions.unionInPlace(action.specifiers)
        handlers.append(action)
    }
    
    func handleUserInput(input: String) {
        let result = interpret(input)
        
        guard result !== Interpretation.invalid else {
            LoggingSystem.instance.addLog("I don't understand")
            return
        }
        
        for handler in handlers {
            handler.handle(result)
        }
    }
    
    // MARK: - Private
    private func interpret(input: String) -> Interpretation {
        var text = input.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var command: String = ""
        var specifier: String = ""
        var primary: [Target] = []
        var secondary: [Target] = []
        
        let searchOptions: NSStringCompareOptions = [NSStringCompareOptions.RegularExpressionSearch, NSStringCompareOptions.CaseInsensitiveSearch]
        guard let commandRange = text.rangeOfString(verbRegex, options: searchOptions) else {
            return Interpretation.invalid
        }
        
        command = text.substringWithRange(commandRange).uppercaseString
        
        text.removeRange(commandRange)
        text = text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        if let specifierRange = text.rangeOfString(prepositionRegex, options: searchOptions) {
            specifier = text.substringWithRange(specifierRange).uppercaseString
            
            var elements: [String] = []
            
            let preText = text.substringWithRange(text.startIndex ..< specifierRange.startIndex)
            if preText.characters.count > 0 {
                elements.append(preText)
            }
            
            let postText = text.substringWithRange(specifierRange.endIndex ..< text.endIndex)
            if postText.characters.count > 0 {
                elements.append(postText)
            }
            
            if let preTargets = elements.first {
                primary = splitTargets(preTargets)
            }
            
            if let postTargets = elements.last where elements.first != elements.last {
                secondary = splitTargets(postTargets)
            }
        } else {
            primary = splitTargets(text)
        }
        
        return Interpretation(command: command, specifier: specifier, primary: primary, secondary: secondary)
    }
    
    private func splitTargets(text: String) -> [Target] {
        guard let regex = try? NSRegularExpression(pattern: "\\b(?i)AND\\b", options: []) else { return [] }
        let nsString = text as NSString
        let splitter = ","
        let modifiedString = regex.stringByReplacingMatchesInString(text, options: [], range: NSRange(location: 0, length: nsString.length), withTemplate: splitter)
        let entries = modifiedString.componentsSeparatedByString(splitter).map({ $0.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) }).filter({ $0 != "" })
        var items: [Target] = []
        for entry in entries {
            let item = mapTarget(entry)
            items.append(item)
        }
        return items
    }
    
    private func mapTarget(input: String) -> Target {
        var words = input.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).filter({ $0.characters.count > 0 })
        
        var noun: Noun?
        for (index, element) in words.enumerate() {
            if let result = LabelSystem.fetchNoun(element) {
                noun = result
                words.removeAtIndex(index)
                break
            }
        }
        
        guard let matchingNoun = noun else { return Target(userInput: input, candidates: []) }
        let entities = LabelSystem.fetchEntitiesForNoun(matchingNoun)
        
        var adjectiveIDs: [Int64] = []
        for word in words {
            if let adjective = LabelSystem.fetchAdjective(word) {
                adjectiveIDs.append(adjective.id)
            }
        }
        
        let candidates = entities.filter { (entity) -> Bool in
            let entityAdjectiveIDs = entity.getAdjectives().map({ $0.id })
            for adjectiveID in adjectiveIDs {
                if !entityAdjectiveIDs.contains(adjectiveID) {
                    return false
                }
            }
            return true
        }
        
        return Target(userInput: input, candidates: candidates)
    }
}