//
//  ReadableSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class ReadableSystem {
    class func setup() {
        InterpreterSystem.instance.register(ReadAction())
    }
    
    class func read(readable: Readable) -> String {
        guard let content = readable.content else { return "I can't read that." }
        return content
    }
}