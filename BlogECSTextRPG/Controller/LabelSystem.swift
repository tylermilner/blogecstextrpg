//
//  LabelSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/16/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class LabelSystem {
    class func fetchNoun(text: String) -> Noun? {
        let textNoCase = Expression<String>("text").collate(.Nocase)
        let table = Noun.table.filter(textNoCase == text)
        guard let match = DataManager.instance.prepare(table).first else { return .None }
        let result = Noun(row: match)
        return result
    }
    
    class func fetchAdjective(text: String) -> Adjective? {
        let textNoCase = Expression<String>("text").collate(.Nocase)
        let table = Adjective.table.filter(textNoCase == text)
        guard let match = DataManager.instance.prepare(table).first else { return .None }
        let result = Adjective(row: match)
        return result
    }
    
    class func fetchEntitiesForNoun(noun: Noun) -> [Entity] {
        let result = Entity.fetchAllByComponentID(Noun.component_id, dataID: noun.id)
        return result
    }
    
    class func describe(entity: Entity) -> String {
        if let description = entity.getNotable()?.description {
            return description
        }
        if let noun = entity.getNouns().first {
            let adjectives = entity.getAdjectives().map({ $0.text }).joinWithSeparator(" ")
            return adjectives.characters.count > 0 ? "\(adjectives) \(noun.text)" : noun.text
        } else {
            fatalError("Cannot describe an entity without a notable or noun component")
        }
    }
}