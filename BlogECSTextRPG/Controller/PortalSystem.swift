//
//  PortalSystem.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class PortalSystem {
    // MARK: - Public
    class func setup() {
        InterpreterSystem.instance.register(PortalAction())
    }
    
    class func activate(portal: Portal) -> Bool {
        guard portal.toRoomID != 0 else {
            return false
        }
        RoomSystem.move(portal.toRoomID)
        return true
    }
    
    class func fetchMatchingPortal(direction: String) -> Portal? {
        guard let room = RoomSystem.room.entity else { return .None }
        let portals = room.getPortals()
        for portal in portals {
            if matchesPortal(portal, direction: direction) {
                return portal
            }
        }
        return .None
    }
    
    // MARK: - Private
    private class func matchesPortal(portal: Portal, direction: String) -> Bool {
        let directions = portal.direction.componentsSeparatedByString(",")
        for path in directions {
            if path == direction {
                return true
            }
        }
        return false
    }
}
