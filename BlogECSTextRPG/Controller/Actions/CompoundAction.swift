//
//  CompoundAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class CompoundAction: BaseAction {
    let actions: [BaseAction]
    let fallback: ((Interpretation) -> ())?
    
    init (actions: [BaseAction], fallback:((Interpretation) -> ())? = .None) {
        self.actions = actions
        self.fallback = fallback
        
        var allCommands: Set<String> = []
        var allSpecifiers: Set<String> = []
        for action in actions {
            allCommands.unionInPlace(action.commands)
            allSpecifiers.unionInPlace(action.specifiers)
        }
        
        super.init(commands: allCommands, specifiers: allSpecifiers, primaryTargetMode: .Any, secondaryTargetMode: .Any)
    }
    
    override func canHandle(interpretation: Interpretation) -> Bool {
        for action in actions {
            if action.canHandle(interpretation) {
                return true
            }
        }
        return false
    }
    
    override func handle(interpretation: Interpretation) {
        for action in actions {
            if action.canHandle(interpretation) {
                action.handle(interpretation)
                return
            }
        }
        defaultFallback(interpretation)
    }
    
    func defaultFallback(interpretation: Interpretation) {
        guard let customFallback = fallback else {
            if commands.contains(interpretation.command) {
                LoggingSystem.instance.addLog(BaseAction.invalidSentence)
            }
            return
        }
        customFallback(interpretation)
    }
}
