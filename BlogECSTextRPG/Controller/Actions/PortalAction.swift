//
//  PortalAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class PortalAction: CompoundAction {
    init() {
        let allCommands = BasePortalAction.commands.union(BasePortalAction.abbreviate.keys)
        let action = BasePortalAction(commands: allCommands, specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
        super.init(actions: [action])
    }
}

private class BasePortalAction: BaseAction {
    static let commands: Set<String> = ["N", "E", "S", "W", "NE", "SE", "SW", "NW", "U", "D"]
    static let abbreviate = [ "NORTH" : "N", "EAST" : "E", "SOUTH" : "S", "WEST" : "W", "NORTHEAST" : "NE", "SOUTHEAST" : "SE", "SOUTHWEST" : "SW", "NORTHWEST" : "NW", "UP" : "U", "DOWN" : "D" ]
    
    private override func handle(interpretation: Interpretation) {
        let command = BasePortalAction.abbreviate[interpretation.command] ?? interpretation.command
        guard commands.contains(command) else { return }
        
        let portal = PortalSystem.fetchMatchingPortal(command)
        guard let match = portal where PortalSystem.activate(match) else {
            let message = portal?.message ?? "You can't go that way."
            LoggingSystem.instance.addLog(message)
            return
        }
    }
}
