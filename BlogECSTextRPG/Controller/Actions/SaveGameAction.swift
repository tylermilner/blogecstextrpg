//
//  SaveGameAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class SaveGameAction: CompoundAction {
    init() {
        let action = BaseSaveGameAction(commands: ["SAVE"], specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
        super.init(actions: [action])
    }
}

private class BaseSaveGameAction: BaseAction {
    override func handle(interpretation: Interpretation) {
        GameSystem.save()
        LoggingSystem.instance.addLog("Saved.")
    }
}
