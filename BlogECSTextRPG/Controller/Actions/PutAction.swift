//
//  PutAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class PutAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["PUT"])
        let noContainer = PutTargetAction(commands: ["PUT"], specifiers: [], primaryTargetMode: .OneOrMore, secondaryTargetMode: .Zero)
        let targetIn = PutTargetInAction(commands: ["PUT"], specifiers: ["IN", "ON"], primaryTargetMode: .OneOrMore, secondaryTargetMode: .Single)
        super.init(actions: [noTarget, noContainer, targetIn])
    }
}

private class PutTargetAction: BaseAction {
    override func handle(interpretation: Interpretation) {
        LoggingSystem.instance.addLog("Where do you want to put it?")
    }
}

private class PutTargetInAction: BaseAction {
    func customTakeFiltering(target: Target) {
        target.candidates = target.candidates.filter({ $0.getTakeable() != nil })
        if target.candidates.count == 0 {
            target.error = "\(target.userInput): You can't do that."
            return
        }
    }
    
    private func customPutFiltering(target: Target) {
        target.candidates = target.candidates.filter({ $0.getContainer() != nil })
        if target.candidates.count == 0 {
            target.error = "You can't put anything in the \(target.userInput)."
            return
        }
    }
    
    override func handle(interpretation: Interpretation) {
        guard let containerTarget = interpretation.secondary.first else { return }
        
        TargetingSystem.filter(containerTarget, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        customPutFiltering(containerTarget)
        TargetingSystem.validate(containerTarget)
        
        guard let containerEntity = containerTarget.match, container = containerEntity.getContainer() where containerTarget.error == nil else {
            guard let error = containerTarget.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        var message: String = ""
        
        for (index, target) in interpretation.primary.enumerate() {
            if index > 0 {
                message += "\n"
            }
            
            TargetingSystem.filter(target, options: TargetingFilter.HeldByPlayer)
            customTakeFiltering(target)
            TargetingSystem.validate(target)
            
            guard let match = target.match, takeable = match.getTakeable() where target.error == nil else {
                if let error = target.error { message += error }
                continue
            }
            
            let result = TakeableSystem.put(takeable, container: container)
            message += "\(target.userInput): \(result)"
        }
        
        LoggingSystem.instance.addLog(message)
    }
}
