//
//  LoadGameAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class LoadGameAction: CompoundAction {
    init() {
        let action = BaseLoadGameAction(commands: ["LOAD"], specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
        super.init(actions: [action])
    }
}

private class BaseLoadGameAction: BaseAction {
    override func handle(interpretation: Interpretation) {
        LoggingSystem.instance.addLog("Loading...")
        GameSystem.load()
    }
}
