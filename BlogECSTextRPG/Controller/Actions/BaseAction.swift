//
//  BaseAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

enum TargetMode {
    case Any        // Matches any case
    case Zero       // Expects exactly zero targets
    case UpToOne    // Expects zero or one target
    case Single     // Expects exactly one target
    case OneOrMore  // Expects one or more targets
    case Multiple   // Expects more than one target
}

class BaseAction: UserAction {
    static let invalidSentence = "That sentence isn't one I recognize."
    let commands: Set<String>
    let specifiers: Set<String>
    let primaryTargetMode: TargetMode
    let secondaryTargetMode: TargetMode
    
    init(commands: Set<String>, specifiers: Set<String>, primaryTargetMode: TargetMode, secondaryTargetMode: TargetMode) {
        self.commands = commands
        self.specifiers = specifiers
        self.primaryTargetMode = primaryTargetMode
        self.secondaryTargetMode = secondaryTargetMode
    }
    
    func canHandle(interpretation: Interpretation) -> Bool {
        guard commands.contains(interpretation.command) else { return false }
        if specifiers.count > 0 && !specifiers.contains(interpretation.specifier) { return false }
        if interpretation.specifier.characters.count > 0 && !specifiers.contains(interpretation.specifier) { return false }
        guard checkMatch(interpretation.primary, mode: primaryTargetMode) else { return false }
        guard checkMatch(interpretation.secondary, mode: secondaryTargetMode) else { return false }
        return true
    }
    
    private func checkMatch(targets: [Target], mode: TargetMode) -> Bool {
        switch mode {
        case .Any:
            return true
        case .Zero:
            return targets.count == 0
        case .UpToOne:
            return targets.count <= 1
        case .Single:
            return targets.count == 1
        case .OneOrMore:
            return targets.count >= 1
        case .Multiple:
            return targets.count > 1
        }
    }
    
    func handle(interpretation: Interpretation) {
        
    }
}
