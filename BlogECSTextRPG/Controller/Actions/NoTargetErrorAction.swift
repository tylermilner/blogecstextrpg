//
//  NoTargetErrorAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class NoTargetErrorAction: BaseAction {
    init(commands: Set<String>) {
        super.init(commands: commands, specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
    }
    
    override func handle(interpretation: Interpretation) {
        let message = "What do you want to \(interpretation.command.lowercaseString)?"
        LoggingSystem.instance.addLog(message)
    }
}
