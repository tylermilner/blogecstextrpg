//
//  InventoryAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class InventoryAction: CompoundAction {
    init() {
        let action = ShowInventoryAction(commands: ["INVENTORY"], specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
        super.init(actions: [action])
    }
}

private class ShowInventoryAction: BaseAction {
    override func handle(interpretation: Interpretation) {
        let items = SightSystem.listContainerContents(PlayerSystem.player)
        guard items.count > 0 else {
            LoggingSystem.instance.addLog("You don't have anything.")
            return
        }
        let message = "You're holding:\n  \(items.joinWithSeparator("\n  "))"
        LoggingSystem.instance.addLog(message)
    }
}
