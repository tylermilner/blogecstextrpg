//
//  OpenAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/20/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class OpenAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["OPEN"])
        let target = OpenTargetAction(commands: ["OPEN"], specifiers: [], primaryTargetMode: .Single, secondaryTargetMode: .Zero)
        super.init(actions: [noTarget, target])
    }
}

class CloseAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["CLOSE"])
        let target = CloseTargetAction(commands: ["CLOSE"], specifiers: [], primaryTargetMode: .Single, secondaryTargetMode: .Zero)
        super.init(actions: [noTarget, target])
    }
}

private class BaseOpenableTargetAction: BaseAction {
    func customOpenFilter(target: Target) {
        target.candidates = target.candidates.filter({ (entity) -> Bool in
            return entity.getOpenable() != nil
        })
        if target.candidates.count == 0 {
            target.error = "You must tell me how to do that to a \(target.userInput)."
            return
        }
    }
    
    func tryAction(target: Target, action: (Openable)->(String)) {
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        customOpenFilter(target)
        TargetingSystem.validate(target)
        
        guard let match = target.match, openable = match.getOpenable() where target.error == nil else {
            LoggingSystem.instance.addLog(target.error!)
            return
        }
        
        let message = action(openable)
        LoggingSystem.instance.addLog(message)
    }
}

private class OpenTargetAction: BaseOpenableTargetAction {
    override private func handle(interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        tryAction(target, action: OpenableSystem.open)
    }
}

private class CloseTargetAction: BaseOpenableTargetAction {
    override private func handle(interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        tryAction(target, action: OpenableSystem.close)
    }
}
