//
//  DropAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class DropAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["DROP"])
        let target = DropTargetAction(commands: ["DROP"], specifiers: [], primaryTargetMode: .OneOrMore, secondaryTargetMode: .Zero)
        super.init(actions: [noTarget, target])
    }
}

private class DropTargetAction: BaseAction {
    override func handle(interpretation: Interpretation) {
        var message: String = ""
        
        for (index, target) in interpretation.primary.enumerate() {
            if index > 0 {
                message += "\n"
            }
            
            TargetingSystem.filter(target, options: TargetingFilter.HeldByPlayer)
            TargetingSystem.validate(target)
            
            guard let match = target.match, takeable = match.getTakeable() where target.error == nil else {
                if let error = target.error { message += error }
                continue
            }
            
            let result = TakeableSystem.drop(takeable)
            message += "\(target.userInput): \(result)"
        }
        
        LoggingSystem.instance.addLog(message)
    }
}
