//
//  LookAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class LookAction: CompoundAction {
    init() {
        let lookAtRoom = LookAtRoom(commands: ["LOOK"], specifiers: [], primaryTargetMode: .Zero, secondaryTargetMode: .Zero)
        let lookAtTarget = LookAtTarget(commands: ["LOOK"], specifiers: ["AT"], primaryTargetMode: .Single, secondaryTargetMode: .Zero)
        let examineTarget = LookAtTarget(commands: ["EXAMINE"], specifiers: [], primaryTargetMode: .Single, secondaryTargetMode: .Zero)
        super.init(actions: [lookAtRoom, lookAtTarget, examineTarget])
    }
}

private class LookAtRoom: BaseAction {
    private override func handle(interpretation: Interpretation) {
        SightSystem.describeRoom(RoomSystem.room, verbose: true)
    }
}

private class LookAtTarget: BaseAction {
    private override func handle(interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom])
        TargetingSystem.validate(target)
        
        guard let match = target.match where target.error == .None else {
            guard let error = target.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        let message = SightSystem.examine(match, userDescription: target.userInput)
        LoggingSystem.instance.addLog(message)
    }
}
