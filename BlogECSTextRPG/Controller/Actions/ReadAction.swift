//
//  ReadAction.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/21/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation

class ReadAction: CompoundAction {
    init() {
        let noTarget = NoTargetErrorAction(commands: ["READ"])
        let target = ReadTargetAction(commands: ["READ"], specifiers: [], primaryTargetMode: .Single, secondaryTargetMode: .Zero)
        super.init(actions: [noTarget, target])
    }
}

class ReadTargetAction: BaseAction {
    private func customReadFilter(target: Target) {
        target.candidates = target.candidates.filter({ (entity) -> Bool in
            return entity.getReadable() != nil
        })
        if target.candidates.count == 0 {
            target.error = "How does one read a \(target.userInput)?"
            return
        }
    }
    
    override func handle(interpretation: Interpretation) {
        guard let target = interpretation.primary.first else { return }
        
        TargetingSystem.filter(target, options: [TargetingFilter.CurrentRoom, TargetingFilter.ContainerIsOpen])
        customReadFilter(target)
        TargetingSystem.filter(target, options: [TargetingFilter.HeldByPlayer])
        TargetingSystem.validate(target)
        
        guard let match = target.match, readable = match.getReadable() where target.error == .None else {
            guard let error = target.error else { return }
            LoggingSystem.instance.addLog(error)
            return
        }
        
        let message = ReadableSystem.read(readable)
        LoggingSystem.instance.addLog(message)
    }
}
