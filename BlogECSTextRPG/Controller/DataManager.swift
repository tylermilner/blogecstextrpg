//
//  DataManager.swift
//  BlogECSTextRPG
//
//  Created by Jonathan Parham on 9/12/16.
//  Copyright © 2016 Jonathan Parham. All rights reserved.
//

import Foundation
import SQLite

class DataManager {
    
    // MARK: - Fields
    private var database: Connection!
    
    // A path to the default database included in the app bundle
    private var defaultDataPath: String {
        get {
            guard let retValue = NSBundle.mainBundle().pathForResource("Zork", ofType: "db") else {
                fatalError("Invalid path to default database File")
            }
            return retValue
        }
    }
    
    // A path to a saved game in progress
    private var userDataPath: String {
        get {
            guard let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first else {
                fatalError("Cannot access documents directory")
            }
            let retValue = NSString(string: documentsPath).stringByAppendingPathComponent("Zork.db")
            return retValue
        }
    }
    
    // A path to an un-saved game in progress
    private var tempDataPath: String {
        get {
            let retValue = NSString(string: NSTemporaryDirectory()).stringByAppendingPathComponent("Zork.db")
            return retValue
        }
    }
    
    // MARK: - Singleton
    static let instance = DataManager()
    private init() {}
    
    // MARK: - Public
    func prepare(table: Table) -> [Row] {
        do {
            let result = try database.prepare(table)
            return Array(result)
        } catch {
            
        }
        return []
    }
    
    func run(update: Update) {
        do {
            try database.run(update)
        } catch {
            
        }
    }
    
    func save() {
        do {
            let userDataURL = NSURL.fileURLWithPath(userDataPath), tempDataURL = NSURL.fileURLWithPath(tempDataPath)
            var newURL: NSURL?
            try NSFileManager.defaultManager().replaceItemAtURL(userDataURL, withItemAtURL: tempDataURL, backupItemName: .None, options: .UsingNewMetadataOnly, resultingItemURL: &newURL)
            load()
        } catch {
            
        }
    }
    
    func load() {
        // If the user has a game in progress, start with that, otherwise begin from the default database
        let path = NSFileManager.defaultManager().fileExistsAtPath(userDataPath) ? userDataPath : defaultDataPath
        
        // Create a copy of the database from the bundle to the users documents folder if necessary
        do {
            if NSFileManager.defaultManager().fileExistsAtPath(tempDataPath) {
                try NSFileManager.defaultManager().removeItemAtPath(tempDataPath)
            }
            try NSFileManager.defaultManager().copyItemAtPath(path, toPath: tempDataPath)
        } catch {
            fatalError("Unable to copy database")
        }
        
        // Connect to the copied database so we are not restricted by read-only mode
        do {
            database = try Connection(tempDataPath)
        } catch {
            fatalError("Unable to connect to database")
        }
    }
    
    func reset() {
        do {
            if NSFileManager.defaultManager().fileExistsAtPath(userDataPath) {
                try NSFileManager.defaultManager().removeItemAtPath(userDataPath)
            }
        } catch {
            
        }
        load()
    }
}
